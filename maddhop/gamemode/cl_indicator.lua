local has_jumped = false
local jumps_counter = 0
local velocity = 0
local in_start = false
local in_end = false

hook.Add('Tick', 'CalculateStats', function()
    local ply = LocalPlayer()
    if ply:IsValid() == false then
        return 
    end

    velocity = math.floor(ply:GetVelocity():Length2D())

    if ply:KeyDown(IN_JUMP) and not ply:OnGround() and not has_jumped then
        jumps_counter = jumps_counter + 1
        has_jumped = true
    elseif ply:OnGround() and has_jumped then
        has_jumped = false
    end

    local player_pos = ply:GetPos()
    if start_zone then
        in_start = player_pos:WithinAABox(start_zone[1], start_zone[2])
    end
    
    if end_zone then
        in_end = player_pos:WithinAABox(end_zone[1], end_zone[2]) 
    end
end)

local bg_color = Color(0, 0, 0, 70)
local text_color = Color(255, 255, 255)
local timer_text = 'None'

function TimeFormatter(seconds)
	local hours = math.floor( seconds / 3600 )
	local minutes = math.floor( ( seconds / 60 ) % 60 )
	local ms = math.floor(( seconds - math.floor(seconds) ) * 10)
	seconds = math.floor( seconds % 60 )

    if (minutes > 0) then
        return string.format('%2i:%02i.%i', minutes, seconds, ms)
    else
        return string.format('%2i.%i', seconds, ms)
    end
end

net.Receive('timer', function ()
    local seconds = net.ReadFloat()
    timer_text = 'Time: ' .. TimeFormatter(seconds)
end)

net.Receive('timer:start', function ()
    timer_text = 'In start zone'
end)

net.Receive('timer:end', function ()
    local seconds = net.ReadFloat()
    timer_text = 'Time: ' .. TimeFormatter(seconds) .. ' (End)'
end)

hook.Add('HUDPaint', 'DrawHudIndicator', function()
    local padding = 20
    local mode_text = 'Normal'
    local velocity_text = 'Velocity: ' .. velocity
    local jumps_text = 'Jumps: ' .. jumps_counter

    local text_arr = {
        mode_text,
        timer_text,
        velocity_text,
        jumps_text
    }
    local text = table.concat(text_arr, '\n')

    local box_width = 220
    local box_height = (19 * #text_arr) + padding * 2 + 1
    local box_x = (ScrW() / 2) - (box_width / 2)
    local box_y = (ScrH() / 2) + 250

    draw.RoundedBox(4, box_x, box_y, box_width, box_height, bg_color)
    draw.DrawText(text, 'HudDefault', ScrW() / 2, box_y + padding, text_color, TEXT_ALIGN_CENTER)
end)
