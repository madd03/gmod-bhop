START_ZONES = {
    bhop_monster_jam = {
        Vector(400, -2190, 3775),
        Vector(880, -2680, 3970),
    },
    bhop_arcane_v1 = {
        Vector(1062, -210, 14400),
        Vector(1370, 203, 14512),
    },
}

END_ZONES = {
    bhop_monster_jam = {
        Vector(5445, -5530, 3770),
        Vector(6120, -4915, 4145)
    },
    bhop_arcane_v1 = {
        Vector(-1039, 1136, 14400),
        Vector(-1458, 2120, 14576),
    },
}

function getStartZone()
    local currentMap = game.GetMap()
    return START_ZONES[currentMap]
end

function getEndZone()
    local currentMap = game.GetMap()
    return END_ZONES[currentMap]
end

function getSpawnPosition()
    local zone = getStartZone()
	return LerpVector(0.5, zone[1], zone[2])
end
