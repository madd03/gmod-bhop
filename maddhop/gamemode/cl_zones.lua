include('zones.lua')

start_zone = getStartZone()
start_color = Color(0, 255, 145)

end_zone = getEndZone()
end_color = Color(220, 0, 0)

local vector_zero = Vector(0, 0, 0)

hook.Add("PostDrawTranslucentRenderables", "DrawZones", function()    
    if start_zone ~= nil then
        render.DrawWireframeBox(vector_zero, angle_zero, start_zone[1], start_zone[2], start_color, true)
    end

    if end_zone ~= nil then
        render.DrawWireframeBox(vector_zero, angle_zero, end_zone[1], end_zone[2], end_color, true) 
    end
end)
