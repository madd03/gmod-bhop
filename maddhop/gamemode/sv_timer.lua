local timers = {}

util.AddNetworkString('timer:start')
util.AddNetworkString('timer:end')
util.AddNetworkString('timer')

function sendTimerStart(ply)
    net.Start('timer:start')
    net.Send(ply)
end

function sendTimerEnd(ply, timer)
    net.Start('timer:end')
    net.WriteFloat(timer)
    net.Send(ply)
end

function sendTimer(ply, timer)
    net.Start('timer')
    net.WriteFloat(timer)
    net.Send(ply)
end

hook.Add('Think', 'ValidateZones', function()
    local players = player.GetAll()

    for _, ply in ipairs(players) do
        if IsValid(ply) then
            trackPlayerTimer(ply)
        end
    end
end)

local startZone = getStartZone()
local endZone = getEndZone()

function playerIsInZone(ply, zone)
    local pos = ply:GetPos()

    if pos:WithinAABox(zone[1], zone[2]) and ply:IsOnGround() then
        return true
    end

    return false
end

function trackPlayerTimer(ply)
    local id = ply:SteamID()
    local inStartZone = playerIsInZone(ply, startZone)
    if inStartZone and not ply:KeyDown(IN_JUMP) then
        timers[id] = {
            timer = 0,
            finished = false
        }

        return sendTimerStart(ply)
    end

    local timer = timers[id]
    if timer and not timer.finished then
        timers[id].timer = timer.timer + FrameTime()
        sendTimer(ply, timer.timer)
    end

    local inEndZone = playerIsInZone(ply, endZone)
    if inEndZone then
        timers[id].finished = true
        sendTimerEnd(ply, timers[id].timer)
    end
end
