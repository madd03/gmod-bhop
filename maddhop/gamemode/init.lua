AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
AddCSLuaFile("cl_autobh.lua")
AddCSLuaFile("cl_indicator.lua")
AddCSLuaFile("commands.lua")
AddCSLuaFile("zones.lua")
AddCSLuaFile("cl_zones.lua")
AddCSLuaFile("player_bhop.lua")

include("shared.lua")
include('zones.lua')
include("sv_timer.lua")

AddCommand('r', function(ply)
	ply:Spawn()
end)

hook.Add('PlayerSpawn', 'SetPlayerClass', function (ply)
	player_manager.SetPlayerClass(ply, 'player_bhop')
	ply:SetTeam(1)
	ply:SetNoCollideWithTeammates(true)
	ply:SetAvoidPlayers(false)
	ply:DrawShadow(false)
	print(CurTime())
end)

function GM:PlayerSwitchWeapon(ply)
	-- fixes issue when CSS weapon is picked up (for some reason it resets player speed)
	timer.Simple(0, function()
		ply:SetWalkSpeed(250)
		ply:SetRunSpeed(250)
	end)
end

function GM:PlayerSelectSpawn(ply)
	local spawnPosition = getSpawnPosition()
	ply:SetPos(spawnPosition)

	return ply
end

function GM:InitPostEntity()
	RunConsoleCommand("sv_airaccelerate", "1000") -- default: 10
	RunConsoleCommand('sv_accelerate', '5') -- default: 5
	RunConsoleCommand("sv_gravity", "800") -- default: 600
	RunConsoleCommand("sv_maxvelocity", "5000") -- default: 3500
	RunConsoleCommand("sv_sticktoground", "0") -- default: 1
	RunConsoleCommand('sv_turbophysics', '1') -- default: 0
	RunConsoleCommand('sv_friction', '4') -- default: 8
end

function GM:PlayerShouldTakeDamage()
	return false
end

function GM:GetFallDamage()
	return false
end

function GM:PlayerSpray()
	return false
end
