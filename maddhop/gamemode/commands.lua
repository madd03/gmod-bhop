local commandHandlers = {}

function handleCommand(ply, text)
    local prefix = text:sub(1, 1)
    if prefix == '!' or prefix == '/' then
        local command = text:sub(2, #text)
        local args = string.Split(command, ' ')
        local commandName = args[1]
        table.remove(args, 1)
        
        local handler = commandHandlers[commandName]
        if not handler then
            return
        end

        local argStr = table.concat(args, ' ') -- to make it compatible with concommand handlers
        handler(ply, commandName, args, argStr)
        if (prefix == '/') then
            return true -- hides message in chat as it supposed with / commands
        end

        return false
    end
end

if SERVER then
    hook.Add('PlayerSay', 'DispatchCommands', function(ply, text)
        local showMsg = handleCommand(ply, text)
        if (showMsg) then
            return ''
        end
    end)
end

if CLIENT then
    hook.Add('OnPlayerChat', 'DispatchClientCommands', function (ply, text)
        if ply ~= LocalPlayer() then return end
        return handleCommand(ply, text)
    end)
end

function AddCommand(commandName, handler)
    commandHandlers[commandName] = handler
    local concommandName = 'gm_' .. commandName
    concommand.Add(concommandName, handler)
end
