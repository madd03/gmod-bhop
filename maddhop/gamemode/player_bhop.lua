DEFINE_BASECLASS('player_bhop')

local PLAYER = {} 
 
PLAYER.WalkSpeed = 250
PLAYER.RunSpeed = 250
PLAYER.JumpPower = 290
PLAYER.CrouchedWalkSpeed = 0.34
PLAYER.DuckSpeed = 0.4
PLAYER.UnDuckSpeed = 0.2

function PLAYER:Loadout()
	self.Player:RemoveAllAmmo()
 	self.Player:GiveAmmo(256, 'Pistol', true)
 	self.Player:Give('weapon_pistol')
	self.Player:Give('weapon_crowbar')
end

player_manager.RegisterClass('player_bhop', PLAYER, 'player_default')
