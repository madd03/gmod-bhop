local ENABLE_AUTOBH = true
local FOOT_WATER_LEVEL = 1

AddCommand('autobh', function()
    ENABLE_AUTOBH = not ENABLE_AUTOBH
end)

hook.Add('StartCommand', 'AutoBunnyhop', function(ply, cmd)
    if not ENABLE_AUTOBH then
        return
    end

    if not ply:Alive() then
        return
    end

    local buttons = cmd:GetButtons()
    if bit.band(buttons, IN_JUMP) ~= 0 then
        if ply:GetMoveType() == MOVETYPE_LADDER then
            return 
        end

        if ply:WaterLevel() > FOOT_WATER_LEVEL then
            return
        end

        if not ply:IsOnGround() then
            cmd:SetButtons(bit.band(buttons, bit.bnot(IN_JUMP)))
        end
    end
end)
