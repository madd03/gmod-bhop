# Garry's Mod Bhop

> Currently I work on improved version - better performance, more reliable start/end zones.

Gmod Bunnyhop custom gamemode made for playing `bhop_` maps locally. Default **Sandbox** gamemode doesn't work well with bunnyhop as it interfere Player's physics.

![](assets/bhop-video.mp4)

> You can preview video on [Imgur](https://imgur.com/a/l2rEnuV).

## Contains:

- Auto Bunnyhop script
- Customized player's velocity and jump height to match Counter-Strike: Source values (it's important, because most `bhop_` maps are ported directly from CS:S).
- Prevents player from taking fall damage (important in `bhop_` maps)
- *Start* and *End* zones and timer
- Commands `!r` for restart and `!autobh` for toggle auto bunnyhop ability
- Stats indicator in HUD: mode, velocity and jumps count

![start-zone](assets/start-zone.jpg)

> Start zone

![timer](assets/timer.jpg)

> Timer

![end-zone](assets/end-zone.jpg)

> End zone and end of timer

## Run gamemode

> I work on more final version that which will be published to the Steam Workshop

#### 1. Clone repository somewhere and copy `maddhop` folder into `<GMOD_FOLDER>/garrysmod/gamemodes`

> **<GMOD_FOLDER>** is root of game's local files.

#### 2. Install `bhop_` map from Workshop

I test gamemode on maps from [BHOP Map Collection](https://steamcommunity.com/sharedfiles/filedetails/?id=380996292). In the future I will probably provide my own ports.


#### 3. Choose "Madd Bhop" gamemode

![Gamemode Chooser](assets/choose-gamemode.png)


## Authors and acknowledgment

Made by [madd](https://steamcommunity.com/id/m4dd1337)
